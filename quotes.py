#!/usr/bin/env python
# Quote of the day - www.101computing.net/quote-of-the-day
import random
import textwrap
#Initialise an empty list that will be stored our bank of quotes
quotes = []

#Append a few quotes to our list
quotes.append("The harder I work, the luckier I get.")
quotes.append("A person who never made a mistake never tried anything new.")
quotes.append("Nothing will work unless you do.")
quotes.append("The best preparation for good work tomorrow is to do good work today.")
quotes.append("Choose a job you love, and you will never have to work a day in your life.")

#Randomly pick a quote from our bank of quotes

dailyQuote = random.choice(quotes)
print(dailyQuote)

